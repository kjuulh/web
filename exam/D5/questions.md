# Exam 5 - Responsive design and testing

## Overview

Redegør for og vis eksempler på udviklingen af en webløsning med responsivt design, samt anvendelse af CSS preprocessors og build tools.

Redegør for og vis eksempler på test af en React App med brug af jest

## Disposition

Responsivt design er en teknik til at lave websites, der kan skalerer mellem forskellige størrelser af devices.

Nogle af de guidelines der kan følges.

1. De forskellige sider skal kunne vises på alle skærm størrelser
2. Indhold bliver præsenteret så det kan vises på alle skærm billeder
3. Der må aldrig blive vist en horizontal scrollbar

Responsive vs adaptive.

Adaptive er f.eks. at detectere forskellige typer skærme, f.eks. www.m.youtube.com

Gør brug af maxwidth og maxheight, så siderne kan flyde frit. Meta tags er vigtigt for startup og skalering.

Mediaqueries bruges til at vælge hvilket markup der bruges.

Til at let kunne organiserer sin css, kan en CSS preprocessor bruges: Sass, less, stylus etc. En preprocessor fungerere ved at bruge et sprog som ikke nødvendigvis kan bruges som rent css, i stedet vil det kunne kompileres til css. En preprocessor giver muligheden for variable, funktioner, mixins etc.

Min favorit er SCSS af SASS, hvilket giver muligden for imports, includes, variable, nesting osv.

Med det her er der brug for build tools. eftersom man hurtigt med kommer til at have mange filer, preprocessors, imports, etc.

Et eksempel kunne være GULP, eller et andet kunne være Webpack (hvilket er en bundler), hvilket er brugt i både CRA og Angular.

Men det er lettere at forstå gulp. Gulp er et build tool og fungerer ved at bruge tasks og pipelines. Man har med gulp, sources, pipes og sinks. pipes kan anvende indbyggede funktioner, samt anvende plugins.

For at teste en applikation kan et testing framework bruges. Jest, kan bruges med andre frameworks, og har funktionalit til at kunne fake, mocke, asserte osv. Desuden tilbyder jest et coverage tool. 
 
Unit testing, snapshot testing.

