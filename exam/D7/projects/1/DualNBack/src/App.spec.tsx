import React from 'react'

import App from "./App";
import {shallow} from "enzyme";

describe('App', () => {
    it('should render app', function () {
        const component = shallow(<App/>);
        expect(component).toMatchSnapshot()
    });
})