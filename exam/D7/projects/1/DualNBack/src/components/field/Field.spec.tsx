import React from 'react'

import {shallow} from "enzyme";
import Field from "./Field";
import {useSelector} from "react-redux";

jest.mock('react-redux', () => ({
    useDispatch: () => {},
    useSelector: jest.fn()
}))

const selectorMock = (field: number, count: number) => {
    useSelector.mockImplementation((state: any) => ({
        field,
        count
    }))
}

describe('Field', function () {
    it('should render field default', function () {
        selectorMock(0, 0);

        const component = shallow(<Field index={0}/>)
        expect(component).toMatchSnapshot();
    });

    it('should have active field', function () {
        selectorMock(1,0)
        const component = shallow(<Field index={0}/>)
        expect(component.find('div.active')).toHaveLength(1);
    });

    it('should have key', function () {
        selectorMock(0, 5)
        const component = shallow(<Field index={0}/> )
        expect(component.key()).toEqual("5")
    });
});
