# Exam 7 - PWA, performance og websockets.

## Overview

Redegør for konceptet Progressive Web Apps og vis hvordan man kan implementerer en Progressive Web App.

Redegøre for hvordan man kan optimere performance for en webapplikation.

Vis hvordan man kan implementere to-vejs data kommunikation ved brug af websockets.

## Disposition

A progressive web app, is a website that is packaged as an app, and will function regardless of screensize, it will work offline. It will refresh in the background. It enables app behavior, such as push notifications. It goes around the app store.

- Progressive
- Responsive
- Connectivity independent
- App-like
- Fresh
- Safe
- Discoverable
- Re-engageable
- Linkable

- App shell
- Service worker

The pages are cached, so that they will function when offline, and load quickly.

se projekt 1.

https://itweb-g12-a3-app.herokuapp.com/

Performance is probably the most important factor of an application, a potential customer will very quickly move on to a better website, if it takes 3 &lt; x seconds. Frontend er største del af grunden. Til at fikse dette, kan man f.eks.

1. Laver færre http requests
2. Bruge et Content delivery network
3. Bruge expires header
4. Putte stylesheets i toppen
5. putte scripts i bunden
6. reducerer dns lookus
7. minify
8. lave ajax cache able

Lighthouse kan vise statisikker omkring ens site, her kan det f.eks. bemærkes hvor hurtigt selve siden loadede, og hvor lang tid der gik inden brugeren kunne bruge sitet. Cachecontrol bruges til cache. no-cache, max-age. Cache for a long time, and change the name of the resource, when needed.

Bundling and minification, tree-shaking etc.

Websockets er en måde at kommunikerer full-duplex, noget som normale http requests ikke understøtter. Det sker ved at et http request er sendt fra en klient til en server, derefter bliver et WebSocket handshake brugt. Efter der er skiftet fra http til websocket, bruges den underlæggende tcp forbindelse. Data bliver sendt i frames, og samlet af modtageren, efter et fin bit modtages.

Se projekt 1.
