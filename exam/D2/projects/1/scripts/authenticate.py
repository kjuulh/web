def authenticate():
    import http.client
    import json
    import pprint

    connection = http.client.HTTPConnection("localhost:8080")

    content = json.dumps({
        'email': "contact@kjuulh.io",
        'password': "Thisisapassword123"
    })

    print("Request")
    print("Body")
    pprint.pprint(json.loads(content))

    headers = {'Content-Type': 'application/json'}
    print()
    print('Headers')
    pprint.pprint(headers)

    connection.request("POST", "/authenticate", content, headers)
    print("\nResponse")
    response = connection.getresponse()
    res = json.loads(response.read().decode())
    pprint.pprint(res)
    print()
    return res


def main():
    authenticate()


if __name__ == "__main__":
    main()
