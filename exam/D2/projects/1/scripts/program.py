import authenticate
import register
import workout


def main():
    register.register()
    token = authenticate.authenticate()['token']
    workout.workout(token)


if __name__ == "__main__":
    main()
