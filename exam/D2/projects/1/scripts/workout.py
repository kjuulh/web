def workout(token):
    import http.client
    import json
    import pprint

    connection = http.client.HTTPConnection("localhost:8080")

    content = json.dumps({
        'name': "This is a workout",
        'description': "This is a description",
    })

    print("Request")
    print("Body")
    pprint.pprint(json.loads(content))

    headers = {'Content-Type': 'application/json',
               'Authorization': f'Bearer {token}'}
    print()
    print('Headers')
    pprint.pprint(headers)

    connection.request("POST", "/workouts", content, headers)
    print("\nResponse")
    response = connection.getresponse()
    pprint.pprint(json.loads(response.read().decode()))


def main():
    workout()


if __name__ == "__main__":
    main()
