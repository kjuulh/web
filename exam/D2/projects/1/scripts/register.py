def register():
    import http.client
    import json
    import pprint

    connection = http.client.HTTPConnection("localhost:8080")

    content = json.dumps({
        'firstName': "Kasper",
        'lastName': "Hermansen",
        'email': "contact@kjuulh.io",
        'password': "Thisisapassword123"
    })

    print("Request")
    print("Body")
    pprint.pprint(json.loads(content))

    headers = {'Content-Type': 'application/json'}
    print()
    print('Headers')
    pprint.pprint(headers)

    connection.request("POST", "/register", content, headers)
    print("\nResponse")
    response = connection.getresponse()
    pprint.pprint(json.loads(response.read().decode()))


def main():
    register()


if __name__ == "__main__":
    main()
