# Exam 2 - NodeJS API, RESTful vs GraphQL

## Overview

Redegør for og vis eksampler på hvordan man kan designe og implementere et WebAPI med Node.js inklusiv persistering af data i en database. Redegør for principperne i et RESTful Web API. Og redegør for fordele og ulemper ved et RESTful Web API i sammenlignet med et GraphQL baseret API.

## Disposition

Med Nodejs og specielt ved at bruge et framework som Express kan der defineres et RESTful api. REST står for Representational State Transfer, og ved at anvende det via et api, giver det adgang til forretningsdomænet. Rest kræver at servicen er stateless og cacheable. Der er nogle krav for et system for at det kan kaldes restful, bla. tilstanden kan være json, xml, html etc. Der kræves at forskellige meta data høre med, HATEOAS gør at den givne resource kan ageres på i forskellige grader, og at dette transmiteres til klienten. Nogle af styrkerne for et rest api er, simplicity, modifiability, visibility, portability og reliability, performance og scalability.

REST vs Gql.

REST er et arkiteknisk koncept for netværk baseret software. GraphQL er et query language. GraphQL.

GraphQL specialisere i at minimere data over wire, ved at kun bede om data der forventes, samt at kunne hente data fra mange kilder og samle dem i sit domæne. Dette betyder at GraphQL nemt kan samle data fra en sql database, en mongodb, et rest api osv. Til at udgive et samlet domæne.

REST er meget simpelt at bruge og kræver bare at der er adgang til http, næsten alle webservere kan udgive et restapi, hvor GraphQL har meget mindre support.

Se eksempel 1.
