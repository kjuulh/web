# Exam 4 - React

## Overview

Redegør for og vis eksampler på hvordan man kan designe og implementere en single-page applikation med brug af React

## Disposition

Til at lave dynamiske websites, kan et såkaldt SPA library bruges, i dette tilfælde bruges React. React er af facebook. React bruger en virtuel dom, hvilket gør at meget af den almene funktionalitet for et website kan ændres til at fungerer anderledes.

React bruges til at vedligeholde et komponentbaseret træ, hvor de forskellige komponenter bruges. React kan ikke så meget selv, men med andre pakker, kan man udvide funktionaliteten, som man har lyst til.

F.eks. routes, http requests, state (flux, observables etc.).

Som et eksempel vises et React projekt.

- Components
  - State
  - lifecycle
  - virtual dom
  - JSX
- Dataflow
  - Props input
  - React elements output
  - Context
- Rendering and lifecycle methods
  - Elements are immutable
  - Functional, class
  - Render is pure
  - Children
  - Events
  - Stateful vs stateless
  - Initialization
  - Mounting
  - Updating
  - Unmounting
- Forms
  - Controlled components (override standard behavior, make react sot)
- Routing
  - Browserrouter, hashrouter
  - Route, switch
  - link, navlink, redicrect
- React testing
  - Jest
  - Enzyme
- React redux
