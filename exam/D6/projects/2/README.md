# Assignment 2

- Group: 12
  - Kasper J. Hermansen, au557919, 201607110
  - Sameer A. Habibi, au543939, 201606697
- Class: ITWEB
- Date: 09-11-2019

## Table of contents

- [Assignment 2](#assignment-2)
  - [Table of contents](#table-of-contents)
  - [Production url](#production-url)
  - [Technological requirements](#technological-requirements)
  - [Purpose](#purpose)
    - [The principles for using a MVC framework on a webserver](#the-principles-for-using-a-mvc-framework-on-a-webserver)
    - [Design and implement a web site that include persistence of data in a database](#design-and-implement-a-web-site-that-include-persistence-of-data-in-a-database)
    - [Hosting the web application in cloud based hosting environments](#hosting-the-web-application-in-cloud-based-hosting-environments)
    - [Authentication and Authorisation](#authentication-and-authorisation)
      - [Authentication](#authentication)
      - [Authorisation](#authorisation)
  - [Install & Run](#install--run)
  - [Contribute](#contribute)

## Production url

- App: [https://itweb-g12-a2-app.herokuapp.com](https://itweb-g12-a2-app.herokuapp.com)
- Api: [https://itweb-g12-a2-api.herokuapp.com](https://itweb-g12-a2-api.herokuapp.com)

## Technological requirements

Node is used as a webserver, with express as an opinionated MVC framework. Pug is used as the templating engine and mongodb (using mongoose) as the database.

Heroku is used as the Cloud hosting provider and Mongodb Atlas as the distributed database provider.

## Purpose

### The principles for using a MVC framework on a webserver

The purpose of using MVC on a webserver is to seperate logic, such as domain, dataaccess and presentation from each other.

The Model layer will contain the domain knowledge, and is the layer that interacts with the data persisted, or not.

The View layer handles the presentation logic, it can be html, json or any other format. The purpose of the format is just that it is seperated from the business logic in the model and isn't tied to the actual handling of the view itself, such as decoding messages from clients or sending data to clients. The view layer should be stateless and only contain logic to enhance the presentation of the contents.

The Controller layer is the layer that binds it together, it is the client facing layer, and acts as an orchestrator in the fact that it acts as a delegating mechanic, which handles incoming requests, calls the model and returns the view, as an example. The controller is sometimes tied to the framework, and can be hard to test, as such it is often kept as thin as possible, to not pollute the Model layer with framework specific code. The Controller should in the best case only serve as an orchestrator, and have as little business logic as possible.

### Design and implement a web site that include persistence of data in a database

To handle the connection to mongodb (Mongo Atlas) in our case, mongoose is used, to provide ease of use, and give a higher level api. Locally a mongodb is spun up with docker to provide a local development environment. Remotely or on in the cloud (heroku) Mongodb Atlas is used, with their free tier sandbox model, which is more than enough for these exercises.

To spin up the local docker image simply run

`./start-mongo.sh # linux / bash`

on windows

`docker run --rm --name mongo -p 27017:27017 mongo`

optionally a local instance of mongodb can be spun up without docker, as long as it is exposed on localhost on port 27017

### Hosting the web application in cloud based hosting environments

Using heroku as a cloud provider, it is simple to use the service. Heroku provides an easy to use pipeline, that automatilly compiles a production build, and starts it, even without the use of docker.

Heroku does have some requirements. The builds scripts in `package.json` has to follow best practices, such as _build_, _start_ etc. 

Heroku also requires a specific variable in code to set the exposed port of the web server.

```javascript
let port = process.env.PORT
if (port == null || port == '') {
  port = '8080'
}
```

Heroku will set the environment variable PORT at runtime, so the server should be able to change dynamically. Heroku will restart the server if neccessary.

### Authentication and Authorisation

Authentication and authorisation are different and used for different use cases, but are often used in the same flow. Authentication will make sure the user is who he/she is. Authorisation makes sure that the user only has the access the user is permitted to. As such authentication is usually used for logon flows, and authorisation is used to choose which content should be available to the user.

In this case authentication and authorisation is used by Json Web Token, which contains a signature that verifies that the user is who they say they are, and can include permissions on which content the user should be able to touch, most often as an id, or a role (RBAC).

#### Authentication

Authentication is the when a server verifies that you're who you say you are.

#### Authorisation

Authorisation, derived from authority, is the role a user has, for example a normal user, has normal user authority or permission and should not be able to delete other users. etc.

## Install & Run

To use the this repo have npm and/or yarn installed and either docker, or mongodb started.

We use yarn, but npm should work just fine.

```
cd itweb-g12-a2-api
./start-mongo.sh # starts mongo container in the current terminal, open another for the rest of the walkthrough
yarn # will get packages
yarn dev # for development
cd ..
cd itweb-g12-a2-app
yarn
yarn dev
```

Open a browser and checkout `localhost:4200`

Should be a success, otherwise open an issue or whisper me at @kjuulh

## Contribute

To contribute simply ask @kjuulh for permission, or fork the repo and open a PR.

The website will automatically be updated when the master branch is updated.