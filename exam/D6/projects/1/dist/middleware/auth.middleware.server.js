"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setIsLoggedIn = function (request, response, next) {
    response.locals.isLoggedIn = request.isAuthenticated();
    next();
};
exports.authGuard = function (request, response, next) {
    if (request.isAuthenticated()) {
        next();
    }
    else {
        response.redirect('/login');
    }
};
//# sourceMappingURL=auth.middleware.server.js.map