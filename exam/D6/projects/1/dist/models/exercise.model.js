"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
exports.ExerciseSchema = new mongoose_1.Schema({
    id: {
        type: mongoose_1.Schema.Types.ObjectId,
    },
    workoutId: {
        required: true,
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Workout',
    },
    name: {
        required: true,
        type: mongoose_1.Schema.Types.String,
        minlength: 1,
    },
    description: {
        type: mongoose_1.Schema.Types.String,
    },
    sets: {
        required: true,
        type: mongoose_1.Schema.Types.Number,
        min: 1,
    },
    reps: {
        type: mongoose_1.Schema.Types.Number,
        min: 1,
    },
    time: {
        type: mongoose_1.Schema.Types.Number,
        min: 1,
    },
    created: {
        type: mongoose_1.Schema.Types.String,
        default: Date.now(),
    },
});
exports.Exercise = mongoose_1.model('Exercise', exports.ExerciseSchema);
//# sourceMappingURL=exercise.model.js.map