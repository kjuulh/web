"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
exports.WorkoutSchema = new mongoose_1.Schema({
    id: {
        type: mongoose_1.Schema.Types.ObjectId,
    },
    ownerId: {
        required: true,
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'User',
    },
    name: {
        required: true,
        type: mongoose_1.Schema.Types.String,
        minlength: 1,
    },
    description: {
        type: mongoose_1.Schema.Types.String,
    },
    exerciseIds: [
        {
            type: mongoose_1.Schema.Types.ObjectId,
        },
    ],
    created: {
        type: mongoose_1.Schema.Types.String,
        default: Date.now(),
    },
});
exports.Workout = mongoose_1.model('Workout', exports.WorkoutSchema);
//# sourceMappingURL=workout.model.js.map