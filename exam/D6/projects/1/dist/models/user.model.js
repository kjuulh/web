"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var bcrypt = require("bcrypt-nodejs");
var mongoose_1 = require("mongoose");
exports.UserSchema = new mongoose_1.Schema({
    firstName: {
        required: true,
        type: mongoose_1.Schema.Types.String,
    },
    lastName: {
        required: true,
        type: mongoose_1.Schema.Types.String,
    },
    email: {
        required: true,
        type: mongoose_1.Schema.Types.String,
    },
    password: {
        required: true,
        type: mongoose_1.Schema.Types.String,
    },
    created: {
        type: mongoose_1.Schema.Types.String,
        default: Date.now(),
    },
});
exports.UserSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
};
exports.UserSchema.methods.validateHash = function (password) {
    try {
        return bcrypt.compareSync(password, this.password);
    }
    catch (error) {
        console.log(error);
        return false;
    }
};
exports.User = mongoose_1.model('User', exports.UserSchema);
//# sourceMappingURL=user.model.js.map