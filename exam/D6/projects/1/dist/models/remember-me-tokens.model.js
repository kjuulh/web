"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
exports.TokenSchema = new mongoose_1.Schema({
    value: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
    userId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    created: {
        type: mongoose_1.Schema.Types.String,
        default: Date.now(),
    },
});
exports.Token = mongoose_1.model('Token', exports.TokenSchema);
//# sourceMappingURL=remember-me-tokens.model.js.map