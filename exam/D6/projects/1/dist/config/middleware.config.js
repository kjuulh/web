"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var auth_middleware_server_1 = require("../middleware/auth.middleware.server");
exports.default = (function (app) {
    app.use(auth_middleware_server_1.setIsLoggedIn);
});
//# sourceMappingURL=middleware.config.js.map