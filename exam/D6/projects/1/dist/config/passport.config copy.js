"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var session = require("express-session");
var passport = require("passport");
exports.default = (function (app) {
    app.use(session({ secret: 'This is ma little secret',
        resave: false })); // TODO: fix
    app.use(passport.initialize());
    app.use(passport.session());
});
//# sourceMappingURL=passport.config copy.js.map