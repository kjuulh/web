"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var path = require("path");
var sass_config_1 = require("./sass.config");
exports.default = (function (app) {
    app.set('views', path.join(__dirname, '../../src/views'));
    app.set('view engine', 'pug');
    sass_config_1.default(app);
    // provide static assets form public
    app.use(express.static(path.join(__dirname, '../../src/public')));
    // Using bootstrap
    app.use('/stylesheets', express.static(path.join(__dirname, '../../node_modules/bootstrap/dist/css')));
    app.use('/stylesheets', express.static(path.join(__dirname, '../../node_modules/@fortawesome')));
    app.use('/scripts', express.static(path.join(__dirname, '../../node_modules/bootstrap/dist/js')));
});
//# sourceMappingURL=static.config.js.map