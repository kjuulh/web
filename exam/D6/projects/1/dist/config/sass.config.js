"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sass = require("node-sass-middleware");
var path = require("path");
exports.default = (function (app) {
    // use sass
    app.use(sass({
        debug: false,
        dest: path.join(__dirname, '../../src/public/stylesheets'),
        force: true,
        prefix: '/stylesheets',
        src: path.join(__dirname, '../../src/sass'),
    }));
});
//# sourceMappingURL=sass.config.js.map