"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var config_1 = require("./config");
exports.default = (function (app) {
    for (var _i = 0, _a = config_1.default.globFiles(config_1.default.routes); _i < _a.length; _i++) {
        var route = _a[_i];
        require(path.resolve(route)).default(app);
    }
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        next(err);
    });
});
//# sourceMappingURL=routes.config.js.map