"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crypto = require("crypto");
var session = require("express-session");
var passport = require("passport");
var passportLocal = require("passport-local");
var passportRememberMe = require("passport-remember-me");
var remember_me_tokens_model_1 = require("../models/remember-me-tokens.model");
var user_model_1 = require("../models/user.model");
exports.default = (function (app) {
    app.use(session({
        resave: true,
        saveUninitialized: true,
        secret: 'This is ma little secret',
        cookie: { maxAge: 365 * 24 * 60 * 60 * 1000 },
    })); // TODO: fix
    app.use(passport.initialize());
    app.use(passport.session());
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });
    passport.deserializeUser(function (id, done) {
        user_model_1.User.findById(id, function (err, user) {
            done(err, user);
        });
    });
    passport.use('local-signup', new passportLocal.Strategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    }, function (req, email, password, done) {
        user_model_1.User.findOne({ email: email }, function (err, user) {
            if (err) {
                return done(err);
            }
            if (user) {
                return done(null, false, req.flash('signupMessage', 'That email is already taken'));
            }
            else {
                var newUser_1 = new user_model_1.User();
                newUser_1.email = email;
                newUser_1.password = newUser_1.generateHash(password);
                newUser_1.firstName = req.body.firstName;
                newUser_1.lastName = req.body.lastName;
                newUser_1.save(function (err) {
                    if (err) {
                        throw err;
                    }
                    return done(null, newUser_1);
                });
            }
        });
    }));
    passport.use('local-login', new passportLocal.Strategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    }, function (req, email, password, done) {
        user_model_1.User.findOne({ email: email }, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, req.flash('loginMessage', 'No user found'));
            }
            if (!user.validateHash(password)) {
                return done(null, false, req.flash('loginMessage', 'Wrong username or password'));
            }
            return done(null, user);
        });
    }));
    passport.use(new passportRememberMe.Strategy(function (token, done) {
        remember_me_tokens_model_1.Token.findOneAndDelete(token, function (err, tokenRes) {
            var userId = tokenRes.userId;
            if (err) {
                return done(err);
            }
            if (!userId) {
                return done(null, false);
            }
            user_model_1.User.findById(userId, function (err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false);
                }
                return done(null, user);
            });
        });
    }, function (user, done) {
        crypto.randomBytes(64, function (err, buf) {
            var token = new remember_me_tokens_model_1.Token({
                value: buf.toString('hex'),
                userId: user._id,
            });
            token.save(function (err) {
                if (err) {
                    return done(err);
                }
                return done(null, token);
            });
        });
    }));
});
//# sourceMappingURL=passport.config.js.map