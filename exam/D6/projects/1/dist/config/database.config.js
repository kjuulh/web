"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var path = require("path");
var config_1 = require("./config");
exports.default = (function (app) {
    for (var _i = 0, _a = config_1.default.globFiles(config_1.default.models); _i < _a.length; _i++) {
        var model = _a[_i];
        require(path.resolve(model));
    }
    if (config_1.default.useMongo) {
        mongoose
            .connect(config_1.default.mongodb, {
            dbName: 'itweb-g12',
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
            .catch(function (err) { return console.log('Error connecting to mongo' + err); });
    }
});
//# sourceMappingURL=database.config.js.map