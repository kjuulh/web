"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var logger = require("morgan");
var database_config_1 = require("./database.config");
var flash_config_1 = require("./flash.config");
var middleware_config_1 = require("./middleware.config");
var parser_config_1 = require("./parser.config");
var passport_config_1 = require("./passport.config");
var routes_config_1 = require("./routes.config");
var static_config_1 = require("./static.config");
function default_1() {
    var app = express();
    database_config_1.default(app);
    app.use(logger('dev'));
    flash_config_1.default(app);
    parser_config_1.default(app);
    passport_config_1.default(app);
    static_config_1.default(app);
    middleware_config_1.default(app);
    routes_config_1.default(app);
    return app;
}
exports.default = default_1;
//# sourceMappingURL=express.config.js.map