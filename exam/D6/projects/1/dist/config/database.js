"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var config_1 = require("./config");
exports.default = (function (app) {
    if (config_1.default.useMongo) {
        console.log(config_1.default.mongodb);
        mongoose
            .connect(config_1.default.mongodb, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            dbName: 'itweb-g12',
        })
            .catch(function (err) { return console.log('Error connecting to mongo' + err); });
    }
});
//# sourceMappingURL=database.js.map