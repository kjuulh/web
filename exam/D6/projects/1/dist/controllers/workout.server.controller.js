"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var exercise_model_1 = require("../models/exercise.model");
var workout_model_1 = require("../models/workout.model");
var WorkoutsController = /** @class */ (function () {
    function WorkoutsController() {
    }
    WorkoutsController.prototype.index = function (req, res, next) {
        res.render('workouts', { title: 'Workouts' });
    };
    WorkoutsController.prototype.myWorkouts = function (req, res, next) {
        workout_model_1.Workout.find({ ownerId: req.user['_id'] }, function (err, workouts) {
            if (err) {
                req.flash('workoutMessages', 'couldn\'t get workouts');
            }
            res.render('myWorkouts', {
                title: 'My Workouts',
                message: req.flash('workoutMessages'),
                workouts: workouts,
            });
        });
    };
    WorkoutsController.prototype.createWorkout = function (req, res, next) {
        console.info('Creating workout');
        var workout = new workout_model_1.Workout({
            ownerId: req.user['_id'],
            name: req.body.name,
            description: req.body.description,
        });
        workout.save(function (err) {
            if (err) {
                console.log('An error occurred saving workout');
                req.flash('workoutMessages', 'Couldn\'t save workout');
            }
            res.redirect('/workouts/mine');
        });
        console.info('Workout Created');
    };
    WorkoutsController.prototype.changeWorkout = function (req, res, next) {
        console.info('Changing workout');
        workout_model_1.Workout.findById(req.params.workoutId, function (err, workout) {
            var userId = req.user['_id'];
            var workoutOwnerId = workout.ownerId;
            if (!userId.equals(workoutOwnerId)) {
                console.log('User doesn\'t have permission');
                res.redirect('/home');
            }
            if (err) {
                console.error(err);
                req.flash('workoutMessages', 'Clouldn\'t update workout');
                res.redirect('/workouts/' + req.params.workoutId);
                return;
            }
            workout.name = req.body.name;
            workout.description = req.body.description;
            workout.save(function (err) {
                if (err) {
                    console.error(err);
                    console.log('An error occurred saving workout');
                    req.flash('workoutMessages', 'Couldn\'t update workout');
                    res.redirect('/workouts/' + req.params.workoutId);
                    return;
                }
                res.redirect('/workouts/mine');
            });
        });
        console.info('Workout Created');
    };
    WorkoutsController.prototype.getWorkout = function (req, res, next) {
        workout_model_1.Workout.findOne({ _id: req.params.workoutId }).exec(function (err, workout) {
            if (err) {
                console.error(err);
                req.flash('workoutMessages', 'Something went wrong');
                res.redirect('/workouts');
                return;
            }
            exercise_model_1.Exercise.find({ workoutId: workout._id }, function (err, exercises) {
                if (err) {
                    console.error('Couldn\'t get exercises');
                }
                else {
                    if (req.isAuthenticated()) {
                        var userId = req.user['_id'];
                        var workoutOwnerId = workout.ownerId;
                        if (userId.equals(workoutOwnerId)) {
                            res.render('editWorkout', {
                                title: 'Edit Workout: ' + workout.name,
                                workout: workout,
                                exercises: exercises,
                            });
                            return;
                        }
                    }
                    res.render('workout', { title: 'Workout: ' + workout.name });
                }
            });
        });
    };
    return WorkoutsController;
}());
exports.default = WorkoutsController;
exports.workoutsController = new WorkoutsController();
//# sourceMappingURL=workout.server.controller.js.map