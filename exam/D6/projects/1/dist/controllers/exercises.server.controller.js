"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var exercise_model_1 = require("../models/exercise.model");
var workout_model_1 = require("../models/workout.model");
var ExercisesController = /** @class */ (function () {
    function ExercisesController() {
    }
    ExercisesController.prototype.createPage = function (req, res, next) {
        workout_model_1.Workout.findOne({ _id: req.params.workoutId }, function (err, workout) {
            if (err) {
                console.error('Invalid workout');
            }
            else {
                res.render('createExercise', {
                    title: 'Create Exercise',
                    message: req.flash('exercisesMessages'),
                    workout: workout,
                });
            }
        });
    };
    ExercisesController.prototype.create = function (req, res, next) {
        workout_model_1.Workout.findOne({ _id: req.params.workoutId }, function (err, workout) {
            if (err) {
                console.error('Invalid workout');
            }
            else {
                var userId = req.user['_id'];
                var workoutOwnerId = workout.ownerId;
                if (!userId.equals(workoutOwnerId)) {
                    res.redirect('/workouts');
                }
                var exercise_1 = new exercise_model_1.Exercise({
                    workoutId: workout._id,
                    name: req.body.name,
                    description: req.body.description,
                    sets: req.body.sets,
                });
                if (req.body.repsOrTime === 'time') {
                    exercise_1.time = req.body.repsTime;
                }
                else {
                    exercise_1.reps = req.body.repsTime;
                }
                exercise_1.save(function (err) {
                    if (err) {
                        console.error(err);
                    }
                    else {
                        workout.exerciseIds.push(exercise_1._id);
                        workout.save(function (err) {
                            if (err) {
                                console.error(err);
                            }
                            else {
                                res.redirect('/workouts/' + workout._id);
                            }
                        });
                    }
                });
            }
        });
    };
    return ExercisesController;
}());
exports.default = ExercisesController;
exports.exercisesController = new ExercisesController();
//# sourceMappingURL=exercises.server.controller.js.map