"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AuthenticationController = /** @class */ (function () {
    function AuthenticationController() {
    }
    AuthenticationController.prototype.login = function (req, res, next) {
        res.render('login', { title: 'Login', message: req.flash('loginMessage') });
    };
    AuthenticationController.prototype.signup = function (req, res, next) {
        res.render('signup', { title: 'Signup', message: req.flash('signupMessage') });
    };
    AuthenticationController.prototype.logout = function (req, res, next) {
        req.logOut();
        res.redirect('/');
    };
    return AuthenticationController;
}());
exports.default = AuthenticationController;
exports.authController = new AuthenticationController();
//# sourceMappingURL=auth.server.controller.js.map