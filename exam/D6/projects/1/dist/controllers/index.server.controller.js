"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var example_model_1 = require("../models/example.model");
var IndexController = /** @class */ (function () {
    function IndexController() {
    }
    IndexController.prototype.index = function (req, res, next) {
        var request = new example_model_1.Example({
            name: req.url,
        });
        request.save();
        res.render('index', { title: 'Home' });
    };
    IndexController.prototype.about = function (req, res, next) {
        res.render('about', { title: 'About' });
    };
    return IndexController;
}());
exports.default = IndexController;
exports.indexController = new IndexController();
//# sourceMappingURL=index.server.controller.js.map