"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var exercises_server_controller_1 = require("../controllers/exercises.server.controller");
var auth_middleware_server_1 = require("../middleware/auth.middleware.server");
var ExercisesRoute = /** @class */ (function () {
    function ExercisesRoute(app) {
        app.route('/workouts/:workoutId/exercises')
            .get(auth_middleware_server_1.authGuard, exercises_server_controller_1.exercisesController.createPage)
            .post(auth_middleware_server_1.authGuard, exercises_server_controller_1.exercisesController.create);
    }
    return ExercisesRoute;
}());
exports.default = ExercisesRoute;
//# sourceMappingURL=exercises.server.route.js.map