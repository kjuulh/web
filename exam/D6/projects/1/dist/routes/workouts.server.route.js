"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var workout_server_controller_1 = require("../controllers/workout.server.controller");
var auth_middleware_server_1 = require("../middleware/auth.middleware.server");
var WorkoutRoute = /** @class */ (function () {
    function WorkoutRoute(app) {
        app.route('/workouts').get(workout_server_controller_1.workoutsController.index);
        app.route('/workouts/mine').get(auth_middleware_server_1.authGuard, workout_server_controller_1.workoutsController.myWorkouts);
        app.route('/workouts').post(auth_middleware_server_1.authGuard, workout_server_controller_1.workoutsController.createWorkout);
        app.route('/workouts/:workoutId')
            .get(workout_server_controller_1.workoutsController.getWorkout);
        app.route('/workouts/:workoutId/put').post(auth_middleware_server_1.authGuard, workout_server_controller_1.workoutsController.changeWorkout);
    }
    return WorkoutRoute;
}());
exports.default = WorkoutRoute;
//# sourceMappingURL=workouts.server.route.js.map