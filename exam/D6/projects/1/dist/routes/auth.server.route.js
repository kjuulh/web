"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var passport = require("passport");
var auth_server_controller_1 = require("../controllers/auth.server.controller");
var AuthRoutes = /** @class */ (function () {
    function AuthRoutes(app) {
        app.route('/login').get(auth_server_controller_1.authController.login);
        app.route('/login').post(passport.authenticate('local-login', {
            successRedirect: '/index',
            failureRedirect: '/login',
            failureFlash: true,
        }));
        app.route('/signup').get(auth_server_controller_1.authController.signup);
        app.route('/signup').post(passport.authenticate('local-signup', {
            successRedirect: '/login',
            failureRedirect: '/signup',
            failureFlash: true,
        }));
        app.route('/logout').get(auth_server_controller_1.authController.logout);
    }
    return AuthRoutes;
}());
exports.default = AuthRoutes;
//# sourceMappingURL=auth.server.route.js.map