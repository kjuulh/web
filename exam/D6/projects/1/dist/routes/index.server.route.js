"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var index_server_controller_1 = require("../controllers/index.server.controller");
var auth_middleware_server_1 = require("../middleware/auth.middleware.server");
var IndexRoute = /** @class */ (function () {
    function IndexRoute(app) {
        app.route('/').get(index_server_controller_1.indexController.index);
        app.route('/index').get(index_server_controller_1.indexController.index);
        app.route('/about').get(auth_middleware_server_1.authGuard, index_server_controller_1.indexController.about);
    }
    return IndexRoute;
}());
exports.default = IndexRoute;
//# sourceMappingURL=index.server.route.js.map