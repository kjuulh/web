# Exam 6 - authentication, authorization and hosting

## Overview

 
Vis hvordan man kan implementere authentication og authorisation i en webløsning. 

Vis hvordan man kan bruge JWT til authentication i et Web API. 

Redegøre for hvordan man kan publicere webløsninger til driftsmiljøer, her under også cloud baseret hosting. 

## Disposition

Authentication går ud på at bevise hvem man er, hvor authorization går ud på at bevise hvad man har rettighed til.

Authentication fungerer ved at man verificerer sin identitet på et tidspunkt. I et klassisk scenarie, vil en bruger indtaste sit password og brugernavn. Serveren vil derefter verificerer denne information og udstede en token eller session, for at vise at denne bruger har adgang til serveren. Klienten kan derefter genbruge denne token/session til at authenticate sig mod serveren. Dette er specielt smart, da serveren ikke behøver at slå op i databasen ved senere anmodninger.

Se projekt 1.

Denne tilgang fungerer for et website, hvor webserveren holder sessionen, dette dur dog ikke hvis man ønsker en stateless applikation. Derfor kan JWT bruges. JWT består af en header, payload (rettigheder) og en signatur. Denne struktur gør at man kan udstede denne JWT hvilket derefter kan bruges via. en http header, og give adgang til et givet api. JWT går ud fra den forudsætning, at header + payload er hashet og signeret, med en hemmelighed som kun serveren har. Derefter via. symmetrisk signering, kan det verificeres af samme secret. eller ved asymmetrisk signering verificeres af en tredje part.

Se projekt 2.

Til at publicerer et website til staging / produktion, skal man sikre sig flere ting. for det første skal man generere et produktions klart byg. Dette byg laver kompilering eller preprocessering på ens kode og danner det mest optimale kode for den hurtigste og sikreste runtime.

Man kan udrulle ens kode på mange måder, enten kan man hoste sin egen service.

https://kjuulh.io/nextcloud

eller via. et static site (gh-pages)

https://kjuulh.github.io/DualNBack/

Men hvis man har brug for noget mere, kunne der bruges Heroku, Netlify etc. Med Heroku er det muligt at lave en pipeline som ser på eventuelle deployments til github, og derefter bygger ens kode, via. variable sat i ens package.json eller procfile. bygget vil derefter kører via. serverless funktioner (paas), for at skalere ens web service.

Med heroku får man bla. logs, metrics, secrets og meget mere.

