# Exam 3 - Angular

## Overview

Redegør for og vis eksampler på hvordan man kan designe og implementereensingle-page applikation med brug af Angular version 7 eller nyere..

## Disposition

Til at lave dynamiske websites, kan et såkaldt SPA framework bruges, i dette tilfælde bruges Angular. Angular er af google, og bruger som regel Typescript af Microsoft. Angular bruger en real dom, hvilket angular gør via. directives, altså ved at se på om det interne model træ har ændret tilstand, og derfor skal opdateres.

Angular kan mange ting og er et framework, som kommer batteries included, derfor tilbyder det bla Komponentbaseret design, et template sprog til html, forskellige metoder og opdeling af moduler. Sammen med services, som eksisterer udenfor komponenter, routing, guards, interceptors, og meget mere er tilgængeligt.

Som et eksempel vises et angular projekt.

- CLI
- Modules
- Components
  - Følger web component standarden.
  - Bruger databinding
    - og events
    - supporterer også two way
- Services
- Routing
- DI
- RxJS
- Reactive Programming
- Angular flex
- Material
- Lifecycles
- Bundling (webpack)
