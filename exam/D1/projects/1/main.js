'use strict';

const http = require('http'),
  httpStatus = require('http-status-codes');

const port = 3000;
const app = http.createServer((req, res) => {
  console.log('Received an incoming request!');
  res.writeHead(httpStatus.OK, {
    'Content-Type': 'text/html',
  });

  let resMsg = '<h1>Hello, World!</h1>';
  res.write(resMsg);
  res.end();
  console.log(`Sent a response: ${resMsg}`);
});

app.listen(port);
console.log(
  `The server has started and is listening on port number: http://localhost:${port}`,
);
