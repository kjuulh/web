# Exam 1 - NodeJS and Express

## Overview

Redegøre for hvordan Node.js kan bruges til at lave en Web server. Og redegør for hvordan Express kan bruges til at strukturerer programmet, så det følger principperne for et server-side MVC framework.

Redegør for hvorfor man behøver en template engine, og vis eksempler på brug af denne.

Samt vis hvordan man kan designe og implementere en webløsning, som omfatter persistering af data i en dokument database med anvendelse af MongoDb

## Disposition

Nodejs er et runtime environment kørende google v8 javascript engine. Det er et event task baseret single threaded eksekveringsmodel som håndterere IO via. queues. Node programmering kræver at der programmeres asynkront for at applikationen er skalerbart, hvilket skal være tilfældet for en webserver. Alt i alt, har nodejs de samme muligheder som et standard programmeringsprog, IO, et fint standard library. Med disse kan man lave en fin webserver. f.eks. via. http pakken.

(se projekt 1.)

En nodejs webserver er en simpel webserver, med meget manuel håndtering. Derfor eksisterede der mange forskellige frameworks/libraries. Express er sådan et. Med Express få man muligheden for at bruge middleware og routing, dette gør at man kan anvende et MVC arkitektonisk mønster.

Med express kan man lave routes der sender til en controller, den controller kan derefter delegerer noget forretningslogik, og derefter gå tilbage til express, hvor der interageres med views, om det er JSON eller en template engine.

En template engine er nødvendigt, hvis man ønsker en behagelig oplevelse med HTML. uden en template engine kræver det utrolig meget coding at have dynamisk html (views).

MongoDB er en nosql database, mongoose bruges til at interagere med denne.

(se projekt 2.)
