"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
let StarsComponent = class StarsComponent {
    constructor() {
        this.count = 5;
        this.rating = 0;
        this.stars = [];
    }
    ngOnInit() {
        for (let i = 1; i <= this.count; i++) {
            this.stars.push(i > this.rating);
        }
    }
};
__decorate([
    core_1.Input()
], StarsComponent.prototype, "count", void 0);
__decorate([
    core_1.Input()
], StarsComponent.prototype, "rating", void 0);
StarsComponent = __decorate([
    core_1.Component({
        selector: "nga-stars",
        templateUrl: "./stars.component.html",
        styleUrls: ["./stars.component.scss"]
    })
], StarsComponent);
exports.StarsComponent = StarsComponent;
//# sourceMappingURL=stars.component.js.map