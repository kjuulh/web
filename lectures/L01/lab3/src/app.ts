import http from "http";
import httpStatus from "http-status-codes";

import test from "./router";

let port = 3000;
const app = http.createServer((req, res) => {
  console.log("Received an incoming request!");
  res.writeHead(httpStatus.OK, {
    "Content-Type": "text/html"
  });

  test();
  const resMsg = "<h1>Hello Node, Typescript speaking!</h1>";
  res.write(resMsg);
  res.end();
  console.log(`Sent a response : ${resMsg}`);
});

app.listen(port);
console.log(`http://localhost:${port}`);
