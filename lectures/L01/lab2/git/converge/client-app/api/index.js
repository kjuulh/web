"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const faker = __importStar(require("faker"));
function randomPost() {
    return {
        id: faker.random.uuid(),
        title: faker.lorem.sentence(),
        content: faker.lorem.paragraphs()
    };
}
const app = express();
app.get("/posts", (_req, res) => {
    const posts = Array.from({ length: 10 }).map(randomPost);
    res.json(posts);
});
app.listen(5000);
console.log("API has started on port 5000");
//# sourceMappingURL=index.js.map