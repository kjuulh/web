"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const link_1 = __importDefault(require("next/link"));
const client_1 = require("@converge/api/client");
const components_1 = require("@converge/design/components");
const Index = ({ posts }) => {
    return (<components_1.Layout>
      <div>
        <h1>Index</h1>
        <ul>
          {posts.map(post => (<li key={post.id}>
              <link_1.default passHref href={`/${post.id}`}>
                <a>{post.title}</a>
              </link_1.default>
            </li>))}
        </ul>
      </div>
    </components_1.Layout>);
};
Index.getInitialProps = async () => {
    const posts = await client_1.apiClient.posts.getListing();
    return { posts };
};
exports.default = Index;
//# sourceMappingURL=index.js.map