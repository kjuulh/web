"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const Heading = styled_components_1.default.h1 `
  font-size: 2rem;
  line-height: 2.4rem;
  margin: 0 0 1rem;
`;
const Main = styled_components_1.default.main `
  padding: 1rem;
`;
function Layout({ children }) {
    return (<Main>
      <Heading>Client app</Heading>
      {children}
    </Main>);
}
exports.Layout = Layout;
//# sourceMappingURL=components.js.map