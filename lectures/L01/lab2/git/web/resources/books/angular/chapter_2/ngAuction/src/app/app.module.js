"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const platform_browser_1 = require("@angular/platform-browser");
const core_1 = require("@angular/core");
const app_routing_module_1 = require("./app-routing.module");
const app_component_1 = require("./app.component");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const home_component_1 = require("./home/home.component");
const carousel_component_1 = require("./carousel/carousel.component");
const footer_component_1 = require("./footer/footer.component");
const navbar_component_1 = require("./navbar/navbar.component");
const product_item_component_1 = require("./product-item/product-item.component");
const product_detail_component_1 = require("./product-detail/product-detail.component");
const search_component_1 = require("./search/search.component");
const product_service_1 = require("./shared/product.service");
const stars_component_1 = require("./stars/stars.component");
let AppModule = class AppModule {
};
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            app_component_1.AppComponent,
            home_component_1.HomeComponent,
            carousel_component_1.CarouselComponent,
            footer_component_1.FooterComponent,
            navbar_component_1.NavbarComponent,
            product_item_component_1.ProductItemComponent,
            product_detail_component_1.ProductDetailComponent,
            search_component_1.SearchComponent,
            stars_component_1.StarsComponent
        ],
        imports: [platform_browser_1.BrowserModule, app_routing_module_1.AppRoutingModule, ng_bootstrap_1.NgbModule],
        providers: [product_service_1.ProductService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map