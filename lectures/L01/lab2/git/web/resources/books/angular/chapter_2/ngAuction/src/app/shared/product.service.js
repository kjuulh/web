"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
let Product = class Product {
    constructor(id, title, price, rating, description, categories) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.rating = rating;
        this.description = description;
        this.categories = categories;
    }
};
Product = __decorate([
    core_1.Injectable({
        providedIn: "root"
    })
], Product);
exports.Product = Product;
class ProductService {
    getProducts() {
        return products.map(p => new Product(p.id, p.title, p.price, p.rating, p.description, p.categories));
    }
    getProductById(productId) {
        return products.find(p => p.id === productId);
    }
}
exports.ProductService = ProductService;
const products = [
    {
        id: 0,
        title: "First Product",
        price: 24.99,
        rating: 4.3,
        description: "This is a short description.",
        categories: ["electronics", "hardware"]
    },
    {
        id: 1,
        title: "Second Product",
        price: 64.99,
        rating: 3.5,
        description: "This is a short description.",
        categories: ["books"]
    }
];
//# sourceMappingURL=product.service.js.map