import { Component, OnInit } from "@angular/core";

@Component({
  selector: "nga-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent implements OnInit {
  collapsed = true;

  constructor() {}

  ngOnInit() {}
}
