const express = require("express");
app = express();

const layouts = require("express-ejs-layouts");
app.set("view engine", "ejs");
app.use(layouts);

const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/capstone", {
  useNewUrlParser: true
});

app.use(express.static("public"));

app.set("port", process.env.PORT || 3000);

app.get("/", (req, res) => {
  res.render("index");
});

app.use(
  express.urlencoded({
    extended: false
  })
);
app.use(express.json());

const subscribersController = require("./controllers/subscriberController");
app.get("/subscribers", subscribersController.getAllSubscribers);
app.get("/contact", subscribersController.getSubscriptionPage);
app.post("/subscribe", subscribersController.saveSubscriber);

const homeController = require("./controllers/homeController");

app.get("/courses", homeController.showCourses);
app.post("/contact", homeController.postedSignUpForm);

// Error handling
const errorController = require("./controllers/errorController");

app.use(errorController.pageNotFoundError);
app.use(errorController.internalServerError);

app.listen(app.get("port"), () => {
  console.log(`Server running at http://localhost:${app.get("port")}`);
});
